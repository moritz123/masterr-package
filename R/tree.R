#'
#' @field tree_type "fast" = own minimal tree implementation, "data.tree" = use data.tree package
#'
#'@export
#'@import data.tree
get_new_node <- function(root_name = "root", tree_type = "speed")
{
  if(tree_type == "speed"){
    tree_node$new(root_name)
  } else if(tree_type == "data.tree"){
    data.tree::Node$new(root_name)
  } else if(tree_type == "space"){
    warning("change code")
    tree_dt$new(root_name, 10000)
  } else if(tree_type == "space_multi_list"){
    warning("change code")
    tree_mult_list$new(root_name, c("feature"))
  }
}

tree_generic <- R6::R6Class("tree_generic",
                       portable = FALSE,
                       class = FALSE,
                       cloneable = FALSE,
                       public = list(
                         tree_obj = NULL,
                         tree_type = NULL,
                         AddChild2 = NULL,
                         initialize = function(tree_type) {
                           self$tree_obj <- get_new_node("root", tree_type)
                           self$tree_type <- tree_type
                           if(tree_type == "space_multi_list"){
                             self$AddChild2 <- function(node, name){
                               tree_obj$AddChild(node, name)
                             }
                           }
                         },
                         AddChild = function(node, name){
                           if(tree_type == "speed" || tree_type == "data.tree"){
                             node$AddChild(name)
                           } else if(tree_type == "space" || tree_type == "space_multi_list"){
                             tree_obj$AddChild(node, name)
                           }
                         },
                         GetAttribute = function(node, attr_name){
                           if(tree_type == "speed" || tree_type == "data.tree"){
                            node[[attr_name]]
                           } else if(tree_type == "space" || tree_type == "space_multi_list"){
                             tree_obj$GetAttribute(node, attr_name)
                           }
                         },
                         SetAttribute = function(node, attr_name, attr_value){
                           if(tree_type == "speed" || tree_type == "data.tree"){
                             node[[attr_name]] <- attr_value
                           } else if(tree_type == "space" || tree_type == "space_multi_list"){
                             tree_obj$SetAttribute(node, attr_name, attr_value)
                           }
                         },
                         bla = list(1,2,3))
)

tree_mult_list <- R6::R6Class("tree_mult_list",
                       portable = FALSE,
                       class = FALSE,
                       cloneable = FALSE,
                       public = list(
                         tree_list = NULL,
                         root_node_id = 1,
                         features = NULL,
                         initialize = function(name, features) {
                           #name,parent,children + length(features)
                           #self$tree_list <- rep(list(list()), 3+length(features))
                           self$features <- features

                           self$tree_list <- vector("list", 3+length(features))
                           names(self$tree_list) <- c("name", "parent", "children", features)
                           self$tree_list[["children"]] <- list()
                           #create root node
                           id <- as.integer(length(self$tree_list[["name"]])+1)
                           self$tree_list[["name"]][[id]] <- name
                           self$tree_list[["parent"]][[id]] <- NA_integer_
                         },
                         AddChild = function(parent, name){
                           id <- as.integer(length(self$tree_list[["name"]])+1)

                           if(!is.integer(parent) || parent < 1 || parent >= id){
                             stop("parent not valid: ", class(parent))
                           }


                           self$tree_list[["name"]][[id]] <- name
                           self$tree_list[["parent"]][[id]] <- parent
                           #sapply(features, function(feature){
                           #  self$tree_list[[feature]][[id]] <- NULL
                           #})

                           #browser()
                           #self$tree_list[["children"]][parent][[1]][length(self$tree_list[["children"]][parent][[1]])+1] <- id
                           if(is.null(self$tree_list[["children"]][parent][[1]])){
                             self$tree_list[["children"]][parent][[1]] <- id
                           } else {
                           self$tree_list[["children"]][[parent]][length(self$tree_list[["children"]][[parent]])+1] <-
                             id
                           }

                           return(id)
                         },
                         GetAttribute = function(node_id = "root", attr_name){
                           if(node_id == "root"){
                             node_id = self$root_node_id
                           }
                           if(!(attr_name %in% attr_namenames(self$tree_list))){
                             stop(attr_name, " unknown node attr!")
                           }
                           self$in_range_stop(node_id)
                           self$tree_list[[attr_name]][[node_id]]
                         },
                         SetAttribute = function(node_id = "root", attr_name, attr_value){
                           if(node_id == "root"){
                             node_id = self$root_node_id
                           }
                           if(!(attr_name %in% names(self$tree_list))){
                             stop(attr_name, " unknown node attr!")
                           }
                           self$in_range_stop(node_id)
                           self$tree_list[[attr_name]][[node_id]] <- attr_value
                         },
                         GetRoot = function(){
                           as.integer(self$root_node_id)
                         },
                         in_range_stop = function(node_id){
                           if(node_id < 1 || node_id > length(self$tree_list[["name"]])) {
                             stop("node_id out of range")
                           }
                         },
                         is_root = function(node_id){
                           return(node_id == self$root_node_id)
                         })
)



#' @export
#' @import data.table
#' @import R6

tree_dt <- R6::R6Class("tree_dt",
                   portable = FALSE,
                   class = FALSE,
                   cloneable = FALSE,
                            public = list(
                              initialize = function(name, buffer_size = 1000) {
                                private$buffer_size <- buffer_size
                                private$extend_dt_buffer()
                                #create root node
                                private$tree_dt[private$root_node_id, "name"] <- name
                              },
                              AddChild = function(parent, name){
                                if(!is.integer(parent) || !self$in_range_check(parent)){
                                  stop("parent not valid: ", class(parent))
                                }
                                if(private$next_id > private$max_id){
                                  private$extend_dt_buffer()
                                }
                                #private$tree_dt[private$next_id, c("parent","name")] <- c(parent, name)
                                private$tree_dt[private$next_id,"parent"] <- parent
                                #private$tree_dt[private$next_id,"children"] <- list()
                                private$tree_dt[private$next_id,"name"] <- name
                                #browser()
                                private$tree_dt[["children"]][[parent]][length(private$tree_dt[["children"]][[parent]])+1] <-
                                  as.integer(private$next_id)

                                private$next_id <- private$next_id + 1
                                return(as.integer(private$next_id - 1))
                              },

                              get_node = function(node_id = "root"){
                                if(node_id == "root"){
                                  node_id = private$root_node_id
                                }
                                self$in_range_stop(node_id)
                                c("node_id" = as.integer(node_id), as.list(private$tree_dt[node_id,]))
                              },

                              get_tree_dt = function(){
                                private$tree_dt
                              },
                              in_range_check = function(node_id){
                                return(node_id < 1 || node_id < nrow(private$tree_dt))
                              },
                              in_range_stop = function(node_id){
                                if(node_id < 1 || node_id > nrow(private$tree_dt)) {
                                  stop("node_id out of range")
                                }
                              },
                              is_root = function(node_id){
                                return(node_id == private$root_node_id)
                              }),
                            private = list(
                              tree_dt = NULL,
                              root_node_id = 1,
                              next_id = 2, #1 is root node
                              max_id = 0,
                              buffer_size = NULL,
                              extend_dt_buffer = function(){
                                #id, parent, children, name
                                private$tree_dt <- rbindlist(list(private$tree_dt, data.table(#id=1:private$buffer_size,
                                                                              parent=integer(private$buffer_size),
                                                                              children=vector('list', private$buffer_size) ,
                                                                              #children=rep(list(list()), private$buffer_size) ,
                                                                              name=character(private$buffer_size),
                                                                              feature=character(private$buffer_size))))
                                private$max_id <- private$max_id + private$buffer_size
                              }
                            )
)


tree_single_list <- R6::R6Class("tree_single_list",
                              portable = FALSE,
                              class = FALSE,
                              cloneable = FALSE,
                              public = list(
                                tree_list = list(),
                                root_node_id = 1,
                                initialize = function(name) {
                                  #create root node
                                  self$tree_list[[1]] <- list("name" = name, "parent" = NA_integer_)
                                },
                                AddChild = function(parent, name){
                                  id <- as.integer(length(self$tree_list)+1)

                                  if(!is.integer(parent) || parent < 1 || parent >= id){
                                    stop("parent not valid: ", class(parent))
                                  }

                                  self$tree_list[[id]] <- list("name" = name, "parent" = parent)

                                  self$tree_list[[parent]][["children"]]

                                  #browser()
                                  self$tree_list[[parent]][["children"]][length(self$tree_list[[parent]][["children"]])+1] <-
                                    id

                                  return(id)
                                },
                                GetAttribute = function(node_id = "root", attr_name){
                                  if(node_id == "root"){
                                    node_id = self$root_node_id
                                  }
                                  self$in_range_stop(node_id)
                                  self$tree_list[[node_id]][[attr_name]]
                                },
                                SetAttribute = function(node_id = "root", attr_name, attr_value){
                                  if(node_id == "root"){
                                    node_id = self$root_node_id
                                  }
                                  self$in_range_stop(node_id)
                                  self$tree_list[[node_id]][[attr_name]] <- attr_value
                                },
                                in_range_stop = function(node_id){
                                  if(node_id < 1 || node_id > nrow(self$tree_dt)) {
                                    stop("node_id out of range")
                                  }
                                },
                                is_root = function(node_id){
                                  return(node_id == self$root_node_id)
                                })
)


tree_benchmark <- function(x = 3000){
  x<- 10000
  test_name <- "asdf"
  depth <- 8
  data_obj <- derand_func(create_synthethic_data, number_of_samples=10000,number_of_attributes=10,
                          values_per_attr=5,number_of_classes=4,depth_of_tree=5,
                          error_prob=0.00)
  feature_domains <- get_feature_domains(data_obj$data[, -ncol(data_obj$data), with=F])
  #-------------------------------------bench data.tree----------------------------------------------
  tictoc::tic("data.tree")
  data_tree <- data.tree::Node$new(test_name)
  build_random_tree(data_tree, feature_domains, depth)
  #for(i in 1:x){
  #  data_tree$AddChild(paste0(test_name,i))
  #}
  tictoc::toc()
  pryr::object_size(data_tree)

  #-------------------------------------bench speed----------------------------------------------
  tictoc::tic("list")
  node <- tree_node$new(test_name)
  build_random_tree(node, feature_domains, depth)
  #for(i in 1:x){
  #  node$AddChild(paste0(test_name,i))
  #}
  tictoc::toc()
  pryr::object_size(node)

  #-------------------------------------bench space----------------------------------------------
  tictoc::tic("table create root")
  tree <- tree_dt$new(test_name, x+1)
  tictoc::toc()
  tictoc::tic("table fill")
  for(i in 1:x){
    #tree$AddChild(tree$get_node()[["node_id"]], test_name)
    tree$AddChild(as.integer(1), paste0(test_name,i))
  }
  #build_random_tree(node, feature_domains, depth)
  tictoc::toc()
  pryr::object_size(tree)
  #View(tree$get_tree_dt())
  #-------------------------------------bench tree_mult_list------------------------------------
  tictoc::tic("tree_mult_list")
  tr_genecric_mult_list <- tree_generic$new("space_multi_list")
  build_random_tree_generic(tr_genecric_mult_list, tr_genecric_mult_list$tree_obj$GetRoot(), feature_domains, depth)

  #tr_mult_list <- tree_mult_list$new(test_name, c("feature"))
  #for(i in 1:x){
  #  tr_mult_list$AddChild(as.integer(1), paste0(test_name,i))
  #}
  tictoc::toc()
  pryr::object_size(tr_genecric_mult_list)
  #-------------------------------------bench tree_single_list------------------------------------
  tictoc::tic("tree_mult_list")
  tr_single_list <- tree_single_list$new(test_name)
  for(i in 1:x){
    tr_single_list$AddChild(as.integer(1), paste0(test_name,i))
  }
  tictoc::toc()
  pryr::object_size(tr_single_list)


  #-------------------------------------bench alt----------------------------------------------
  tictoc::tic("pure_list")
  pure_node <- tree_pure_list$new("root")
  for(i in 1:x){
    #pure_node$AddChild(pure_node$get_node()[["node_id"]], "child")
    pure_node$AddChild(as.integer(1), "child")
  }
  tictoc::toc()
  pryr::object_size(node)

  #-------------------------------------bench test----------------------------------------------
  tictoc::tic("data.table fill")
  dt <- data.table::data.table(parent=integer(x),
    children=vector('list',x) ,
    name=character(x))
  i<-1
  for(i in 1:x){
    #dt[i, "parent"] <- x-1
    #dt[i, "name"] <- "x*2"
    dt[i, list("parent","name")] <- c(x-1, "x*2")
    #dt[i, "c"][[length(dt[i, "c"])+1]] <- "hi"
    #dt[i, "d"] <- "bla"
    #dt[i,] <- c(as.integer(3), list(list()), "bla")
  }
  tictoc::toc()

}

#' @export
#' @import data.table
#' @import R6
tree_node <- R6::R6Class("tree_node",
                     portable = FALSE,
                     class = FALSE,
                     cloneable = FALSE,
                   public = list(
                     name = NULL,
                     children = NULL,
                     parent = NULL,
                     feature = NULL,
                     obsCount = NULL,
                     initialize = function(name = "", parent = NULL) {
                       self$name <- name
                       self$parent <- parent
                     },
                     AddChild = function(name) {
                       new_node <- tree_node$new(name, self)
                       #edge-case children with same name already exists
                       self$children[[length(self$children)+1]] <- new_node
                       names(self$children)[length(self$children)] <- name
                       return(new_node)
                       #browser()
                     }
                     )
                   )

#' @export
#' @import data.table
#' @import R6
tree_pure_list <- R6::R6Class("tree_pure_list",
                   portable = FALSE,
                   class = FALSE,
                   cloneable = FALSE,
                   public = list(
                     node_list = list(),
                     root_node_id = 1,

                     initialize = function(name) {
                       id <- length(node_list)+1
                       #browser()
                       self$node_list[[id]] <- list(name = name, parent = NULL, children = NULL, feature = NULL)
                     },
                     AddChild = function(parent, name){
                       if(!is.integer(parent) || !self$in_range_check(parent)){
                         stop("parent not valid: ", class(parent))
                       }
                       id <- length(node_list)+1
                       new_node <- list(name = name, parent = parent, children = NULL, feature = NULL)
                       self$node_list[[id]] <- new_node
                       #browser()
                       self$node_list[[parent]][["children"]][length(self$node_list[[parent]][["children"]])+1] <- id

                       return(as.integer(id))
                     },
                     get_node = function(node_id = "root"){
                       if(node_id == "root"){
                         node_id <- self$root_node_id
                       }
                       self$in_range_stop(node_id)
                       c("node_id" = as.integer(node_id), node_list[[node_id]])
                     },
                     in_range_check = function(node_id){
                       return(node_id < 1 || node_id <= length(self$node_list))
                     },
                     in_range_stop = function(node_id){
                       if(!in_range_check(node_id)) {
                         stop("node_id out of range")
                       }
                     },
                     is_root = function(node_id){
                       return(node_id == self$root_node_id)
                     })
)
