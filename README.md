### What is this repository for? ###

This repository contains plots and functions for the master thesis "Differential Privacy in the context of Tree Classification".

### How do I get set up? ###

Download and install with devtools::install_bitbucket("moritz123/masterr-package", build_vignettes = TRUE).

Tested with R version >= 3.4.0

Read the vignette for further instructions and examples.

Further informations on r-packages visit http://r-pkgs.had.co.nz